function imdb = casia_webface_get_database(webfaceDir, varargin)
% Get imdb struct for CASIA WebFace database.
%   This assumes the following directory structure:
%   webfaceDir/names.txt
%   webfaceDir/CASIA-WebFace
%   webfaceDir/CASIA-WebFace-resized

opts.seed = 0 ;
opts.overlapFile = 'casia-janus-overlap.txt' ; 
opts.dbType = 'raw';
opts.useVal = true;
opts = vl_argparse(opts, varargin) ;
rng(opts.seed) ;

fid = fopen(fullfile(webfaceDir, 'names.txt'));
filelist = textscan(fid, '%s %s');
fclose(fid);
personID = filelist{1}; personName = filelist{2};


% remove class overlaps with other dataset
if ~isempty(opts.overlapFile)
    [personName, trimListIndex] = getTrimmedList(personName, webfaceDir, opts.overlapFile);
    personID = personID(~trimListIndex);
end


switch opts.dbType
    case 'raw'
        imgDir = 'CASIA-WebFace';
    case 'resized'
        imgDir = 'CASIA-WebFace-resized'; % resized to 100x100 
    otherwise
        error('Incorrect database type: must be raw or resized');
end


% Images and class
imdb.imageDir = fullfile(webfaceDir, imgDir);
imdb.images.name = {};
imdb.classes.name = personName;
imdb.classes.description = [];
imageLabel = {};

for c = 1:length(personID)
    personDir = fullfile(webfaceDir, imgDir, personID{c});
    listing = dir(fullfile(personDir, '*.jpg'));
    fprintf('\nDirectory: %s Name: %s', personID{c}, personName{c});
    for j = 1:length(listing)
        imdb.images.name{end+1, 1} = ...
                            sprintf('%s/%s', personID{c}, listing(j).name);
        % img = imread(fullfile(imdb.imageDir, imdb.images.name{end})); % debugging
        imageLabel{end+1} = personID{c};
    end
end

imdb.images.id = 1:length(imdb.images.name);
[~, imdb.images.label] = ismember(imageLabel, personID);


% No standard image splits are provided for this dataset
% N.B. - CASIA WebFace data is not used for testing performance
imdb.sets = {'train', 'val', 'test'};
imdb.images.set = ones(1,length(imdb.images.id)); % all in train set

% val/test set - 20% of training data from each class
% !!! CHANGED - 1 image from each class is VAL
if opts.useVal
    for c = 1:length(imdb.classes.name),
        classSet = (imdb.images.label==c);
        classTrainSet = classSet .* (imdb.images.set==1);
        classTrainSet = find(classTrainSet);
%         valSet = classTrainSet(floor(4*end/5)+1: end);
%         imdb.images.set(valSet) = 2;  % val set
        valSet = classTrainSet(end);
        imdb.images.set(valSet) = 2;
    end
end

% make this compatible with the OS imdb
imdb.meta.classes = imdb.classes.name ;
imdb.meta.inUse = true(1,numel(imdb.meta.classes)) ;




% helper function to remove classes overlapping with other datasets
function [namelist, trimListIndex] = getTrimmedList(namelist, facescrubDir, overlapFile)
fid = fopen(fullfile(facescrubDir, overlapFile));
overlist = textscan(fid, '%s', 'Delimiter', '\n');
overlist = overlist{1};
trimListIndex = {};
for i = 1:numel(overlist) 
    t = cellfun(@(x)(~isempty(strfind(x, overlist{i}))), ...
                                        namelist, 'un', 0);   
    trimListIndex{i} = cell2mat(t);
end    
trimListIndex = cell2mat(trimListIndex);
trimListIndex = sum(trimListIndex, 2);
trimListIndex(trimListIndex~=0) = 1;
namelist = namelist(~trimListIndex);
