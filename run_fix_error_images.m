function run_fix_error_images(varargin)
%RUN_FIX_ERROR_IMAGES Replace error images with resized originals
%   IMDB is the struct generated by MatConvNet from the MatConvNet Imagenet
%   example.
    
c = 0;
opts.keepAspect = true;
opts.imageSize = [256, 256];
opts.interpolation = 'bicubic';
opts.imdbPath = '/data/arunirc/cnn-verydeep/data/imagenet12-vgg-vd-16-bnorm-simplenn/imdb.mat';
opts.origDirPath = '/data/arunirc/datasets/ImageNet/ILSVRC2012/CLS-LOC/images';

opts = vl_argparse(opts, varargin);

assert(any(exist(opts.imdbPath, 'file')));
assert(any(exist(opts.origDirPath, 'dir')));
imdb = load(opts.imdbPath);

for ii = 1:numel(imdb.images.name)
   imPath = fullfile(imdb.imageDir, imdb.images.name{ii});
   % img = vl_imreadjpeg({imPath}) ; 
   % img = img{1};
   fprintf('Image: %d/%d\n', ii, numel(imdb.images.name));
 
   try
       img = imread(imPath);
   catch
       % If image cannot be read from IMDB path, then replace with original
       % images, after resizing it correctly.
       img = imread(origDirPath, imdb.images.name{ii});
       
       % resize
       w = size(img,2) ;
       h = size(img,1) ;
       factor = [opts.imageSize(1)/h, opts.imageSize(2)/w];
       if opts.keepAspect
         factor = max(factor) ;
       end
       if any(abs(factor - 1) > 0.0001)
         img = imresize(img, ...
                       'scale', factor, ...
                       'method', opts.interpolation) ;
       end
       imwrite(img, imPath);
       
       c = c + 1;
       disp(c);
   end
end
fprintf('\n%d corrupted images replaced with resized original images.\n', c);

