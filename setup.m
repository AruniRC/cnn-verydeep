
run('../vlfeat/toolbox/vl_setup');
run matconvnet-1.0-beta17/matlab/vl_setupnn;

addpath matconvnet-1.0-beta17/examples/
addpath matconvnet-1.0-beta17/examples/imagenet

clear mex ;

%{
run('../vlfeat/toolbox/vl_setup');
run matconvnet-1.0-beta17/matlab/vl_setupnn;
addpath(genpath(fullfile('matconvnet-1.0-beta17', 'examples', 'imagenet')));
% addpath matlab-helpers
clear mex ;
%}



%{
vl_compilenn('enableGpu', true, 'cudaMethod', 'nvcc', ...
'cudaRoot', '/usr/local/cuda', ...
'enableCudnn', true, ...
'cudnnRoot', '/home/arunirc/Downloads/cudnn-7.0-v3', ...
'enableImreadJpeg', true, 'Verbose', 1) ;

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/lib64
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cudnn-6.5-linux-x64-v2
%}
