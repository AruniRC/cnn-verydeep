function imdb = ijba_get_database( protocolDir, faceDir, split, modelTrainSet)
%JANUS_GET_DATABASE_TRAIN Returns IMDB struct for IJB-A sets.
%
%   protocolDir points to the location of the IJB-A metadata root folder
%   E.g. protocolDir = '/home/arunirc/datasets/IJB-A/IJB-A_1N_sets'; % on
%   Kepler
%
% 	faceDir points to folder of all cropped face images in IJB-A
%
%   modelTrainSet specifies which set from 'train' or'gallery' is used to 
%   train the model. It could be 'train' if this IMDB is used to fine-tune
%   a CNN, or 'gallery' if this IMDB is used to train SVMs for matching
%   Probe images to Gallery.
   

 if nargin <= 4
    modelTrainSet = 'gallery';  %'gallery' or 'train'
 end

imdb.imageDir = faceDir;

    
% -------------------------------------------------------------------------
%                       Protocol: Classification with 1 out of 10 splits
% -------------------------------------------------------------------------
% Sets:
%   Train - 
%   Gallery - Training set 
%   Probe - Testing set

splitFolder = sprintf('split%d', split);

% -------------------------------------------------------------------------
%                                                  CS1 Train/Gallery set
% ------------------------------------------------------------------------- 
% Train metadata (set = 1) 
if isequal(modelTrainSet, 'gallery')
    modelTrainSet = sprintf('search_gallery');    
end
    
fid = fopen(fullfile(protocolDir, splitFolder, ...
                        sprintf('%s_%d.csv', modelTrainSet, split) ) );        
[filenames, templateID, classLabel, subjectNames, mediaID] =  ijba_read_split(fid);
fclose(fid); 

% Image names
imdb.images.name = filenames; 
imdb.images.template = templateID;    
imdb.images.label = classLabel;
imdb.images.media = mediaID;
imdb.images.set = ones(1,length(filenames));
subjects = subjectNames;
trainSubjectNames = unique(subjectNames);

% -------------------------------------------------------------------------
%                                                              Probe set
% -------------------------------------------------------------------------
% Probe metadata (set = 3)

if isequal(modelTrainSet, 'train')
    probeSet = 0;
else
    probeSet = 3;
end


fid = fopen(fullfile(protocolDir, splitFolder, ...
                        sprintf('search_probe_%d.csv', split) ) );
[filenames, templateID, classLabel, subjectNames, mediaID] =  ijba_read_split(fid);
fclose(fid);

imdb.images.name = vertcat(imdb.images.name, filenames);
imdb.images.template = horzcat(imdb.images.template, templateID);
imdb.images.label = horzcat(imdb.images.label, classLabel);
imdb.images.media = horzcat(imdb.images.media, mediaID);
imdb.images.set = horzcat(imdb.images.set, probeSet*ones(1,length(filenames)));
subjects = vertcat(subjects, subjectNames);
probeSubjectNames = unique(subjectNames);


imdb.sets = {'train', 'val', 'test'};  
imdb.images.id = (1:numel(imdb.images.name));   
imdb.classes.name = unique(subjects);        
[~, classNames] = ismember(subjects, unique(subjects));
imdb.images.label = classNames'; % row vector


% make this compatible with the OS imdb
imdb.meta.classes = imdb.classes.name ;
imdb.meta.inUse = true(1,numel(imdb.meta.classes)) ;
imdb.images.difficult = false(1, numel(imdb.images.id)) ;   

% TODO - separate classes in Gallery from Probe (open-set protocol)

     


% -------------------------------------------------------------------------
function [filenames, templates, labels, subjectNames, media] =  ijba_read_split(fid)
% -------------------------------------------------------------------------
% Read split metadata and return imdb struct
  
    metadata = textscan(fid, strtrim(repmat('%s ', 1, 25)), ...
                                'Delimiter', ',', 'HeaderLines', 1);
    
    templateID = str2double(metadata{1});
    subjectNames = metadata{2};
    sightingID = metadata{5}; % keep as string for filename reads
    mediaID = str2double(metadata{4});
    
    % Face-image filenames are SIGHTING_ID.jpg
    filenames = strcat(sightingID, '.jpg');
    templates = templateID';
    media = mediaID';
    
    % Class labels
    [~, classLabel] = ismember( subjectNames, ...
                                    unique(subjectNames) );
    labels = reshape(classLabel, 1, numel(classLabel));


