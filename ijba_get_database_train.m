function imdb = ijba_get_database_train( protocolDir, faceDir, split, modelTrainSet)
%JANUS_GET_DATABASE_TRAIN Returns IMDB struct for IJB-A sets.
%
%   protocolDir points to the location of the IJB-A metadata root folder
%   E.g. protocolDir = '/home/arunirc/datasets/IJB-A/IJB-A_1N_sets'; % on
%   Kepler
%
% 	faceDir points to folder of all cropped face images in IJB-A
%
%   modelTrainSet specifies which set from 'train' or'gallery' is used to 
%   train the model. It could be 'train' if this IMDB is used to fine-tune
%   a CNN, or 'gallery' if this IMDB is used to train SVMs for matching
%   Probe images to Gallery.
opts.useVal = true;   

 if nargin < 4
    modelTrainSet = 'gallery';  %'gallery' or 'train'
 end

imdb.imageDir = faceDir;
rng(0);

    
% -------------------------------------------------------------------------
%                       Protocol: Classification with 1 out of 10 splits
% -------------------------------------------------------------------------
% Sets:
%   Train - 
%   Gallery - Training set 
%   Probe - Testing set

splitFolder = sprintf('split%d', split);

% -------------------------------------------------------------------------
%                                                  CS1 Train/Gallery set
% ------------------------------------------------------------------------- 
% Train metadata (set = 1) 
if isequal(modelTrainSet, 'gallery')
    modelTrainSet = sprintf('search_gallery');    
end
    
fid = fopen(fullfile(protocolDir, splitFolder, ...
                        sprintf('%s_%d.csv', modelTrainSet, split) ) );        
[filenames, templateID, classLabel, subjectNames, mediaID] =  ijba_read_split(fid);
fclose(fid); 

% Image names
imdb.images.name = filenames; 
imdb.images.template = templateID;    
imdb.images.label = classLabel;
imdb.images.media = mediaID;
imdb.images.set = ones(1,length(filenames)); % (set = 1)
subjects = subjectNames;
trainSubjectNames = unique(subjectNames);


imdb.images.id = (1:numel(imdb.images.name));   
imdb.classes.name = unique(subjects);     
imdb.classes.description = [];
[~, classNames] = ismember(subjects, unique(subjects));
imdb.images.label = classNames'; % row vector


% No standard image splits are provided for this dataset
imdb.sets = {'train', 'val'};
imdb.images.set = ones(1,length(imdb.images.id));
if opts.useVal
    for c = 1:length(imdb.classes.name),
        isclass = find(imdb.images.label == c);
    
        % val set - 20% of training data from each class
        order = randperm(length(isclass));
        subsetSize = ceil(0.8*length(order));
        train = isclass(order(1:subsetSize));
        val = isclass(order(subsetSize+1:end));

        imdb.images.set(train) = 1;
        imdb.images.set(val) = 2;
    end
end

% make this compatible with the OS imdb
imdb.meta.classes = imdb.classes.name ;
imdb.meta.inUse = true(1,numel(imdb.meta.classes)) ;
imdb.images.difficult = false(1, numel(imdb.images.id)) ;   

     


% -------------------------------------------------------------------------
function [filenames, templates, labels, subjectNames, media] =  ijba_read_split(fid)
% -------------------------------------------------------------------------
% Read split metadata and return imdb struct
  
    metadata = textscan(fid, strtrim(repmat('%s ', 1, 25)), ...
                                'Delimiter', ',', 'HeaderLines', 1);
    
    templateID = str2double(metadata{1});
    subjectNames = metadata{2};
    sightingID = metadata{5}; % keep as string for filename reads
    mediaID = str2double(metadata{4});
    
    % Face-image filenames are SIGHTING_ID.jpg
    filenames = strcat(sightingID, '.jpg');
    templates = templateID';
    media = mediaID';
    
    % Class labels
    [~, classLabel] = ismember( subjectNames, ...
                                    unique(subjectNames) );
    labels = reshape(classLabel, 1, numel(classLabel));


