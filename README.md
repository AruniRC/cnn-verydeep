The M-file `run_imdb_train.m` is the starting point to train on your custom dataset.

It exposes settings such as levels data augmentation, batch normalization, etc.

Please note, custom code for the DAG branch has not yet been written.
