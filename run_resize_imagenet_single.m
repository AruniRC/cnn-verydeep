function run_resize_imagenet_single( imdb, dstDir )
%RUN_RESIZE_IMAGENET Summary of this function goes here
%   Detailed explanation goes here

    chatfieldResize = false;


    % imdbPath = '/data/arunirc/cnn-verydeep/data/imagenet12-vgg-vd-16-bnorm-simplenn';

    % img_list = dir(fullfile(imgDir));

    images = imdb.images.name(imdb.images.set==2) ;
    
    % c = makecform('cmyk2srgb'); % handle CMYK jpeg images 
    % fid = fopen('data/imagenet_cmyk.txt', 'a');
       
    parfor ii = 1:numel(images)
        
        fprintf('%d\n',ii);

        img = vl_imreadjpeg({strcat([imdb.imageDir filesep],images{ii})}) ; 
        img = img{1};
                   
        sz = size(img);
        
        % Chatfield14. Resize the smaller side to be 224, then take center
        % crops, when no data augmentation is being done.
        if chatfieldResize
            S = 224;
        else
            S = 256;
        end
        if sz(1) < sz(2)
            imgOut = imresize(img, [S NaN], 'method', 'bicubic');
        else
            imgOut = imresize(img, [NaN S], 'method', 'bicubic');
        end

        [a,b,c] = fileparts(images{ii});
        vl_xmkdir(fullfile(dstDir,a));

        imwrite(imgOut, fullfile(dstDir, images{ii}), 'Quality', 100);
    end
end

