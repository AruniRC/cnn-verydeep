function imdb = vgg_face_get_database(dataDir, varargin)
% Get imdb struct for the VGG-FACE-JANUS database.
%   This assumes the following directory structure:
%   webfaceDir/names.txt
%   webfaceDir/CASIA-WebFace
%   webfaceDir/CASIA-WebFace-resized

opts.seed = 0 ;
opts.useVal = true;
% dataDir = '/data/arunirc/datasets/VGG-Face-dataset/vgg-face-janus';
opts = vl_argparse(opts, varargin) ;
rng(opts.seed) ;

metadata = load(fullfile(dataDir, 'imdb.mat'));

imdb.imageDir = fullfile(dataDir, 'images');
imdb.images = metadata.imdb.images;
imdb.classes = metadata.imdb.classes;
imdb.sets = {'train', 'val', 'test'};

% val/test set - 20% of training data from each class
if opts.useVal
    imdb.images.set(imdb.images.set==3) = 2; % make test set into val
%     for c = 1:length(imdb.classes.name),
%         classSet = (imdb.images.label==c);
%         classTrainSet = classSet .* (imdb.images.set==1);
%         classTrainSet = find(classTrainSet);
%         valSet = classTrainSet(floor(4*end/5)+1: end);
%         imdb.images.set(valSet) = 2;  % val set
%     end
end

% make this compatible with the OS imdb
imdb.meta.classes = imdb.classes.name ;
imdb.meta.inUse = true(1,numel(imdb.meta.classes)) ;

