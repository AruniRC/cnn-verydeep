function imdb_cnn_train(varargin)
%CNN_CASIA   Demonstrates training a CNN on a custom dataset.
%  This demo suitably modifies the MatConvNet example cnn_imagenet.m to
%  train a VD-16 model with data augmentation on images.

% run(fullfile(fileparts(mfilename('fullpath')), ...
%   '..', '..', 'matlab', 'vl_setupnn.m')) ;

opts.dataset = 'casia' ;
opts.dataDir = fullfile('data','CASIA') ;
opts.seed = 0;
opts.protocolDir = '';
opts.modelPath = '';
opts.modelType = 'alexnet' ;
opts.networkType = 'simplenn' ;
opts.batchNormalization = true ;
opts.weightInitMethod = 'gaussian' ;
opts.transformation = 'none'; % EDIT
opts.makeBorder = true ; % EDIT
opts.randomCrop = true ; % EDIT
opts.rgbJitter = true ; % EDIT
opts.keepAspect = true ;
opts.printDatasetInfo = false; %EDIT
[opts, varargin] = vl_argparse(opts, varargin) ;

sfx = opts.modelType ;
if opts.batchNormalization, sfx = [sfx '-bnorm'] ; end
sfx = [sfx '-' opts.networkType] ;
if opts.seed
   opts.expDir = fullfile('data', ...
                    [opts.dataset '-' num2str(opts.seed) '-' sfx]) ; 
else
   opts.expDir = fullfile('data', [opts.dataset sfx]) ;
end
[opts, varargin] = vl_argparse(opts, varargin) ;

opts.numFetchThreads = 12 ;
opts.lite = false ;
opts.imdbPath = fullfile(opts.expDir, 'imdb.mat');
% opts.train = struct('gpus', [1]) ;
opts.train = struct('gpus', [1,2,3,4]) ;
opts.train.batchSize = 128;
[opts, varargin] = vl_argparse(opts, varargin) ;


% -------------------------------------------------------------------------
%                                                                 Load data
% -------------------------------------------------------------------------
if exist(opts.imdbPath)
  imdb = load(opts.imdbPath) ;
else
  switch opts.dataset
    case 'cubcrop'
        imdb = cub_get_database(opts.dataDir, true);
    case 'cub'
        imdb = cub_get_database(opts.dataDir, false);
    case 'facescrub'
        imdb = facescrub_get_database(opts.dataDir) ;
    case 'casia'
        imdb = casia_webface_get_database(opts.dataDir);
    case 'ijba-train'
        imdb = ijba_get_database_train(opts.protocolDir, opts.dataDir, ...
                                        opts.seed, 'train') ;    
    case 'vgg-face'
        imdb = vgg_face_get_database(opts.dataDir);
    otherwise
        error('Unknown dataset %s', opts.dataset) ;
  end   
  mkdir(opts.expDir) ;
  save(opts.imdbPath, '-struct', 'imdb') ;
end

if opts.printDatasetInfo
  print_dataset_info(imdb) ;
end


% -------------------------------------------------------------------------
%                                                             Prepare model
% -------------------------------------------------------------------------
if isempty(opts.modelPath)
    net = cnn_init('model', opts.modelType, ...
                            'batchNormalization', opts.batchNormalization, ...
                            'weightInitMethod', opts.weightInitMethod, ...
                            'networkType', opts.networkType, ...
                            'numClass', numel(imdb.classes.name) ) ;
else
    % initialize with a pre-trained model
    assert(any(exist(opts.modelPath)));
    net = load(opts.modelPath); 
    net = cnn_init('model', 'init', ...
                   'net', net, ...
                   'batchNormalization', opts.batchNormalization, ...
                   'weightInitMethod', opts.weightInitMethod, ...
                   'networkType', opts.networkType, ...
                   'numClass', numel(imdb.classes.name) ) ;

    % modify learning rates for fine-tuning
    net.layers{end-1}.learningRate =  [100 200]; % higher LR for random init
    lr = 0.001 * logspace(-1, -4, 5) ; % reduced LR for fine-tuning 
    net.meta.trainOpts.learningRate = lr ;
    net.meta.trainOpts.numEpochs = numel(lr) ;
end

% -------------------------------------------------------------------------
%                                                              Prepare data
% -------------------------------------------------------------------------

% Set the class names in the network
net.meta.classes.name = imdb.classes.name ;
net.meta.classes.description = imdb.classes.description ;

% Compute image statistics (mean, RGB covariances, etc.)
imageStatsPath = fullfile(opts.expDir, 'imageStats.mat') ;
if exist(imageStatsPath)
  load(imageStatsPath, 'averageImage', 'rgbMean', 'rgbCovariance') ;
else
  [averageImage, rgbMean, rgbCovariance] = getImageStats(opts, net.meta, imdb) ;
  save(imageStatsPath, 'averageImage', 'rgbMean', 'rgbCovariance') ;
end

% Set the image average (use either an image or a color)
%net.meta.normalization.averageImage = averageImage ;
net.meta.normalization.averageImage = rgbMean ;

% Set data augmentation statistics
if opts.rgbJitter
    [v,d] = eig(rgbCovariance) ;
    net.meta.augmentation.rgbVariance = 0.1*sqrt(d)*v' ;
    clear v d ;
else
    net.meta.augmentation.rgbVariance = [] ; % no colour jittering option
end

% Meta parameters - EDIT
net.meta.augmentation.transformation = opts.transformation; % EDIT
net.meta.trainOpts.batchSize = opts.train.batchSize;
net.meta.normalization.keepAspect = opts.keepAspect;

% -------------------------------------------------------------------------
%                                                                     Learn
% -------------------------------------------------------------------------

switch opts.networkType
  case 'simplenn', trainFn = @cnn_train ;
  case 'dagnn', trainFn = @cnn_train_dag ;
end

[net, info] = trainFn(net, imdb, getBatchFn(opts, net.meta), ...
                      'expDir', opts.expDir, ...
                      net.meta.trainOpts, ...
                      opts.train) ;

% -------------------------------------------------------------------------
%                                                                    Deploy
% -------------------------------------------------------------------------

net = cnn_imagenet_deploy(net) ;
modelPath = fullfile(opts.expDir, 'net-deployed.mat')

switch opts.networkType
  case 'simplenn'
    save(modelPath, '-struct', 'net') ;
  case 'dagnn'
    net_ = net.saveobj() ;
    save(modelPath, '-struct', 'net_') ;
    clear net_ ;
end

% -------------------------------------------------------------------------
function fn = getBatchFn(opts, meta)
% -------------------------------------------------------------------------
useGpu = numel(opts.train.gpus) > 0 ;

bopts.numThreads = opts.numFetchThreads ;
bopts.imageSize = meta.normalization.imageSize ;
bopts.border = meta.normalization.border ;
bopts.averageImage = meta.normalization.averageImage ;
bopts.rgbVariance = meta.augmentation.rgbVariance ;
bopts.transformation = meta.augmentation.transformation ;
bopts.randomCrop = opts.randomCrop ;
bopts.keepAspect = opts.keepAspect ;

switch lower(opts.networkType)
  case 'simplenn'
    fn = @(x,y) getSimpleNNBatch(bopts,x,y) ;
  case 'dagnn'
    fn = @(x,y) getDagNNBatch(bopts,useGpu,x,y) ;
end

% -------------------------------------------------------------------------
function [im,labels] = getSimpleNNBatch(opts, imdb, batch)
% -------------------------------------------------------------------------
images = strcat([imdb.imageDir filesep], imdb.images.name(batch)) ;
isVal = ~isempty(batch) && imdb.images.set(batch(1)) ~= 1 ;

if ~isVal
  % training
  im = imdb_get_batch(images, opts, ...
                      'prefetch', nargout == 0) ;
else
  % validation: disable data augmentation
  im = imdb_get_batch(images, opts, ...
                              'prefetch', nargout == 0, ...
                              'transformation', 'none') ;
end

if nargout > 0
  labels = imdb.images.label(batch) ;
end

% -------------------------------------------------------------------------
function inputs = getDagNNBatch(opts, useGpu, imdb, batch)
% -------------------------------------------------------------------------
images = strcat([imdb.imageDir filesep], imdb.images.name(batch)) ;
isVal = ~isempty(batch) && imdb.images.set(batch(1)) ~= 1 ;

if ~isVal
  % training
  opts.prefetch = (nargout == 0) ;
  im = cnn_imagenet_get_batch(images, opts) ;
else
  % validation: disable data augmentation
  opts.prefetch = (nargout == 0) ;
  opts.transformation = 'none' ;
  im = cnn_imagenet_get_batch(images, opts) ;
end

if nargout > 0
  if useGpu
    im = gpuArray(im) ;
  end
  labels = imdb.images.label(batch) ;
  inputs = {'input', im, 'label', imdb.images.label(batch)} ;
end

% -------------------------------------------------------------------------
function [averageImage, rgbMean, rgbCovariance] = getImageStats(opts, meta, imdb)
% -------------------------------------------------------------------------
train = find(imdb.images.set == 1) ;
train = train(1: 101: end);
bs = 256 ;
opts.networkType = 'simplenn' ;
fn = getBatchFn(opts, meta) ;

avg = {}; rgbm1 = {}; rgbm2 = {};

for t=1:bs:numel(train)
  batch_time = tic ;
  batch = train(t:min(t+bs-1, numel(train))) ;
  fprintf('collecting image stats: batch starting with image %d ...', batch(1)) ;
  temp = fn(imdb, batch) ;
  z = reshape(permute(temp,[3 1 2 4]),3,[]) ;
  n = size(z,2) ;
  avg{end+1} = mean(temp, 4) ;
  rgbm1{end+1} = sum(z,2)/n ;
  rgbm2{end+1} = z*z'/n ;
  batch_time = toc(batch_time) ;
  fprintf(' %.2f s (%.1f images/s)\n', batch_time, numel(batch)/ batch_time) ;
end
averageImage = mean(cat(4,avg{:}),4) ;
rgbm1 = mean(cat(2,rgbm1{:}),2) ;
rgbm2 = mean(cat(3,rgbm2{:}),3) ;
rgbMean = rgbm1 ;
rgbCovariance = rgbm2 - rgbm1*rgbm1' ;
