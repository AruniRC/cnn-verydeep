function run_imagenet_val()
% Pre-trained VGG very-deep-16 from MatConvNet beta-17 on Imagenet data 

expDir = 'data/imagenet12-vgg-16-beta17-val_v17';
modelPath = 'data/models/imagenet-matconvnet-vgg-verydeep-16.mat';
dataDir = '/data/arunirc/datasets/ImageNet/ILSVRC2012_resized_256_single/CLS-LOC';
% dataDir = '/data/arunirc/datasets/ImageNet/ILSVRC2014/CLS-LOC/'; % original
% dataDir = '/home/arunirc/datasets/Imagenet/ILSVRC2014/CLS-LOC/'; % 256x256

vl_xmkdir(expDir);

% using beta-17 for eval
info = cnn_imagenet_evaluate('dataDir', dataDir, ...
                            'modelPath', modelPath, ...
                            'expDir', expDir);

disp(info);
save(fullfile(expDir, 'result.mat'), '-struct', 'info');
fprintf('\nvalidation: top1e:%.3f top5e: %.3f\n', info.val.error(1,end), ...
                                                    info.val.error(2,end)); 
%{
% Using beta-16 for eval
cnn_imagenet_val('dataDir', '/home/arunirc/datasets/Imagenet/ILSVRC2014/CLS-LOC', ...
             'modelPath', 'data/models/imagenet-matconvnet-vgg-verydeep-16.mat', ...
             'expDir', 'data/imagenet12-vgg-16-beta17-val', ...
             'networkType', 'simplenn', ...
             'transformation', 'none', ...
             'batchNormalization', false);
%}
         
         
         
         
         
         
function cnn_imagenet_val(varargin)         
% CNN_IMAGENET_VAL  Run a trained CNN model on ImageNet validation set

opts.dataDir = fullfile('data','ILSVRC2012') ;
opts.modelPath = 'alexnet' ;
opts.networkType = 'simplenn' ;
opts.batchNormalization = false ;
opts.expDir = 'data/imagenet12-alexnet' ;
opts.numFetchThreads = 12 ;
opts.lite = false ;
opts.transformation = 'none' ;
[opts, varargin] = vl_argparse(opts, varargin) ;
opts.imdbPath = fullfile(opts.expDir, 'imdb.mat');
opts = vl_argparse(opts, varargin) ;

opts.train.numEpochs = 1 ;
opts.train.batchSize = 128 ; % EDIT
opts.train.numSubBatches = 1 ;
opts.train.continue = true ;
opts.train.gpus = [1,2,3,4] ; % EDIT
opts.train.prefetch = true ;
opts.train.sync = true ;
opts.train.cudnn = true ;
opts.train.expDir = opts.expDir ;
opts.train.memoryMapFile = fullfile('/home/arunirc/tmpspmd', 'matconvnet.bin') ;         


% -------------------------------------------------------------------------
%                                             Load the network and database
% -------------------------------------------------------------------------

if exist(opts.imdbPath)
  imdb = load(opts.imdbPath) ;
else
  imdb = cnn_imagenet_setup_data('dataDir', opts.dataDir, 'lite', opts.lite) ;
  mkdir(opts.expDir) ;
  save(opts.imdbPath, '-struct', 'imdb') ;
end

if exist(opts.modelPath)
    net = load(opts.modelPath);
    if isfield(net, 'meta') % beta-17 net has a different structure
        net.normalization = net.meta.normalization;
    end
else
    error('Model path %s does not exist.', opts.modelPath);
end


% -------------------------------------------------------------------------
%                                                Validation set performance
% -------------------------------------------------------------------------
bopts = net.normalization ;
bopts.numThreads = opts.numFetchThreads ;
bopts.transformation = opts.transformation ;
opts.train.numEpochs = 1 ;
useGpu = numel(opts.train.gpus) > 0 ;


% set CNN_TRAIN to EVALUATE_MODE
imdb.images.set(imdb.images.set==1) = 0; % remove training set

switch lower(opts.networkType)
  case 'simplenn'
    fn = getBatchSimpleNNWrapper(bopts) ;
    
    % CNN_TRAIN removes output of final layer if it's not a loss
    net.layers{end}.type = 'softmaxloss';
    net.layers{end}.name = 'loss';
    [~,info] = cnn_train(net, imdb, fn, opts.train, ...
                                'conserveMemory', true, ...
                                'freezeBnorm', true) ;                          
    save(fullfile(opts.expDir, 'result.mat'), '-struct', 'info');
    fprintf('\nvalidation: top1e:%.3f top5e: %.3f\n', info.val.error(1,end), ...
                                                    info.val.error(2,end)); 
  case 'dagnn'
    fn = getBatchDagNNWrapper(bopts, useGpu) ;
    opts.train = rmfield(opts.train, {'sync', 'cudnn'}) ;
    info = cnn_train_dag(net, imdb, fn, opts.train) ;
    save(fullfile(opts.expDir, 'result.mat'), '-struct', 'info');
    fprintf('\nvalidation: top1e:%.3f top5e: %.3f\n', info.val.error(1,end), ...
                                                    info.val.error(2,end)); 
end




% -------------------------------------------------------------------------
function fn = getBatchSimpleNNWrapper(opts)
% -------------------------------------------------------------------------
fn = @(imdb,batch) getBatchSimpleNN(imdb,batch,opts) ;

% -------------------------------------------------------------------------
function [im,labels] = getBatchSimpleNN(imdb, batch, opts)
% -------------------------------------------------------------------------
images = strcat([imdb.imageDir filesep], imdb.images.name(batch)) ;
im = cnn_imagenet_get_batch(images, opts, ...
                            'prefetch', nargout == 0) ;
labels = imdb.images.label(batch) ;

% -------------------------------------------------------------------------
function fn = getBatchDagNNWrapper(opts, useGpu)
% -------------------------------------------------------------------------
fn = @(imdb,batch) getBatchDagNN(imdb,batch,opts,useGpu) ;

% -------------------------------------------------------------------------
function inputs = getBatchDagNN(imdb, batch, opts, useGpu)
% -------------------------------------------------------------------------
images = strcat([imdb.imageDir filesep], imdb.images.name(batch)) ;
im = cnn_imagenet_get_batch(images, opts, ...
                            'prefetch', nargout == 0) ;
if nargout > 0
  if useGpu
    im = gpuArray(im) ;
  end
  inputs = {'input', im, 'label', imdb.images.label(batch)} ;
end